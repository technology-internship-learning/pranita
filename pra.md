  # GitLab
  ![](./Image/./GitLab%20/git/output.jpg) 



GitLab is a web-based DevOps lifecycle tool that provides a Git repository manager providing wiki, issue-tracking, and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc.

## 4. Creating your own SSH keys.

_Firstly why do we need a SSH key?_

_-SSH keys are used to establish a secure and authenticated connection between your local machine and the GitLab server. If you have a SSH keys you won't have to use your username and password everytime in order to connect to GitLab._

### First: Run your command prompt.

- Write this code to generate a RSA format key which is a valid format for the SSH key in GitLab, other options are ECDSA, ED25519, ECDSA_SK, or ED25519_SK. 

- DON'T FORGET TO ENTER YOUR EMAIL IN WHERE MY EMAIL IS ENTERED.

- Don't forget the patheway to where your SSH key is saved.

![](./Image/GitLab%20/git/ssh5.png)

### Second: Follow your SSH key patheway.

- Remember that your SSH key is saved in a .ssh folder.

![](./Image/GitLab%20/git/ssh3.png)

![](./Image/GitLab%20/git/ssh4.png)

### Third: Get to your GitLab Account.

- Login into your GitLab account.
- Click to your avatar and click to edit profile option.
- In the side bar, you will find a optgion for adding a new SSH key.
- Where you have to paste the generated SSH key.
- Add the usage, dates, and a name.
- And your SSH key is generated and saved to your GitLab account.

![](./Image/GitLab%20/git/ssh6.png)

![](./Image/GitLab%20/git/ssh7.png)

### Fourth: How to get your SSH keys into works?

- You go to your projects and click to the code option and copy the the clone with SSH URL.
- Then create a new folder wherever you like and open it in command prompt.
- Then type "git clone" and paste the copied URL from your GitLab account. 
- Now your project is Git pulled from the source into your laptop.

![](./Image/GitLab%20/git/ssh8.png)

![](./Image/GitLab%20/git/ssh9.png)

















